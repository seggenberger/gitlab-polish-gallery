require 'nokogiri'
require_relative 'models/ui_polish'

class Parser
  def initialize(merge_request)
    @merge_request = merge_request
  end

  def parse
    images = before_and_after_images
    return nil if images.empty?

    UIPolish.new(
      title: merge_request.title,
      username: merge_request.username,
      avatar_url: merge_request.avatar_url,
      url: merge_request.url,
      milestone: merge_request.milestone,
      images: images,
      labels: merge_request.labels,
    )
  end

  private

  attr_reader :merge_request

  def before_and_after_images
    doc = Nokogiri::HTML(merge_request.description_html)

    headers = get_headers(doc)
    return [] unless headers.size > 0 && headers.include?(:before) && headers.include?(:after)

    image_sets = []

    doc.xpath('//*/table/tbody/tr').each_with_index do |row, i|
      image_sets[i] = {}

      row.xpath('td').each_with_index do |td, j|
        src = get_image_src(td)
        next unless src

        image_sets[i][headers[j]] = src
      end
    end

    image_sets.select { |i| !i.empty? }
  end

  def get_headers(doc)
    headers = []

    doc.xpath('//*/table/thead/tr/th').each do |th|
      headers << th.text.downcase.to_sym
    end

    headers
  end

  def get_image_src(td)
    return unless td.css('img') && td.css('img').first && td.css('img').first.attributes && td.css('img').first.attributes["data-src"]

    url = td.css('img').first.attributes["data-src"].value

    url.start_with?("https://gitlab.com", "http://gitlab.com") ? url : "https://gitlab.com" + url
  end
end
