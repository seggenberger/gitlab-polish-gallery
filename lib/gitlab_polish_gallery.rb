require 'erb'

require_relative 'models/merge_request'
require_relative 'parser'

class GitlabPolishGallery
  def initialize(fetcher)
    @polishes = fetcher.fetch.map { |m| Parser.new(m).parse }.compact
    @count = @polishes.count
    @count_by_author = @polishes.group_by { |p| p.username }.count
    @milestones = @polishes.group_by { |p| p.milestone }.keys
    @labels = MergeRequest::LABELS
  end

  def write_html
    File.write('public/index.html', html)
  end

  private

  def html
    template = File.join(File.dirname(__FILE__), 'templates/index.erb')
    ERB.new(File.read(template)).result(binding)
  end
end
