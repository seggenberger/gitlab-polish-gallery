require_relative 'models/merge_request'

class GitlabFetcher
  def self.fetch
    MergeRequest.fetch
  end
end
