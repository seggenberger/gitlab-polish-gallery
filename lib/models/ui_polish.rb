class UIPolish
  attr_accessor :title, :username, :url, :images, :milestone, :avatar_url, :labels

  def initialize(title:, username:, url:, milestone:, avatar_url:, images: [], labels: [])
    @title = title
    @username = username
    @url = url
    @milestone = milestone
    @images = images
    @avatar_url = avatar_url
    @labels = labels
  end
end
