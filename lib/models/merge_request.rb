# fetches list of merged MRs which have rspec profiling label

require 'json'
require 'net/http'

class MergeRequest
  URL = URI('https://gitlab.com/api/graphql')
  HEADER = {'Content-Type' => 'application/json'}
  MAX_PAGE = 50
  LABELS = {
    "UI polish" => "info",
    "Beautifying our UI" => "success",
    "UX Paper Cuts" => "tier"
  }
  QUERY_TPL = <<EOF
{
  project(fullPath: "gitlab-org/gitlab") {
    mergeRequests(labels: "%{label}", state: merged, sort: MERGED_AT_DESC, after: "%{after}", mergedAfter: "2021-01-01") {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        mergedAt
        webUrl
        title
        descriptionHtml
        milestone {
          title
        }
        author {
          username
          avatarUrl
        }
        labels {
          nodes {
            title
          }
        }
      }
    }
  }
}
EOF

  def initialize(params)
    @title = params["title"]
    @username = params["username"]
    @avatar_url = params["avatar_url"]
    @milestone = params["milestone"]
    @url = params["url"]
    @description_html = params["description_html"]
    @labels = params["labels"]
  end

  attr_reader :title, :username, :url, :description_html, :milestone, :avatar_url, :labels

  def self.fetch(since: nil)
    fetch_merge_requests.map do |h|
      avatar_url = h["author"]["avatarUrl"]

      unless avatar_url.start_with?('https://') || avatar_url.start_with?('http://')
        avatar_url = 'https://gitlab.com' + avatar_url
      end

      fetchLabels = h["labels"]["nodes"]
      labels = {}

      fetchLabels.each do |fetchLabel|
        labelName = fetchLabel["title"]
        if LABELS[labelName]
          labels = labels.merge({labelName => LABELS[labelName]})
        end
      end

      params = h.merge({
        "username" => h["author"]["username"],
        "avatar_url" => avatar_url,
        "url" => h["webUrl"],
        "description_html" => h["descriptionHtml"],
        "milestone" => h.dig("milestone", "title"),
        "labels" => labels,
      })
      new(params)
    end
  end

  def self.fetch_merge_requests
    mrs = []

    LABELS.each do |label, value|
      cursor = ''
      MAX_PAGE.times.each do |times|
        puts "Fetching #{label} MRs, cursor: #{cursor}"
        template = QUERY_TPL % {after: cursor, label: label}
        request_data = { query: template }.to_json
        response = Net::HTTP.post(URL, request_data, HEADER)
        json = JSON.parse(response.body)

        mrs.concat(json.dig('data', 'project', 'mergeRequests', 'nodes'))

        page_info = json.dig('data', 'project', 'mergeRequests', 'pageInfo')
        break unless page_info['hasNextPage']

        cursor = page_info['endCursor']
      end
    end

    mrs.uniq { |mr| mr["iid"] }
  end

  def to_json(*a)
    {
      json_class: self.class.name,
      title: @title,
      username: @username,
      avatar_url: @avatar_url,
      milestone: @milestone,
      url: @url,
      description_html: @description_html,
      labels: @labels,
    }.to_json(*a)
  end
end
