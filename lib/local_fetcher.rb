require_relative 'gitlab_fetcher'
require_relative 'models/merge_request'

class LocalFetcher
  LOCAL_SEED_FILE = 'tmp/mrs.json'

  def self.fetch
    if File.exists?(LOCAL_SEED_FILE)
      return JSON.parse(File.read(LOCAL_SEED_FILE)).map { |h| MergeRequest.new(h) }
    else
      data = GitlabFetcher.fetch
      File.write(LOCAL_SEED_FILE, data.to_json)

      data
    end
  end

  def self.cleanup
    FileUtils.rm(LOCAL_SEED_FILE) if File.exists?(LOCAL_SEED_FILE)
  end
end
