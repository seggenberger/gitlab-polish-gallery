# GitLab UI Polish Gallery

Hello 👋 this is a project to collect all the great small UI improvements we do at GitLab.
If you have questions, want to help (e.g. sponsor a nicer design for the page 😁) or have ideas how to improve, please create an issue or ping @nicolasdular.

[I  WANT TO SEE THE GALLERY NOW](https://nicolasdular.gitlab.io/gitlab-polish-gallery/)


## How does it work?

All Merge Requests that have the label `UI polish` set and have before and after images in a table will show up here. Here is an example MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/53213.

You can use normal markdown tables, as long as you mark "Before" and "After" columns correctly. There is also no limit on the amount of rows.

## Development

```
bundle install
ruby bin/server
```

This starts a server that fetches all merge requests temporarily and regenerates the `index.html` on each reload http://localhost:9090/. You can then modify the `lib/templates/index.erb` file.

## How and what to contribute?

Just open a merge request, if you have an idea! A few things that I have in mind right now that would be great to pick up:
- Improve the design of the page
- Make the side-by-side comparison of the images easier to see (e.g. show them in a modal that pops up)
- Create a page with the most upvoted UI improvements
- Create a page with the most contributors (Hall of Fame)


